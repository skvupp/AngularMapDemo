import { Injectable } from '@angular/core';
import {LatLngLiteral} from '@agm/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import {delayWhen, tap} from 'rxjs/operators';
import {timer} from 'rxjs/observable/timer';
import 'rxjs/add/operator/retryWhen';

@Injectable()
export class AddressService {

  constructor(private http: HttpClient) { }

  getAddress(coords: LatLngLiteral): Observable<Marker> {
    const url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + coords.lat + ',' + coords.lng;
    return this.http.get(url)
        .map(res => {
          if (res['status'] !== 'OK') {
            throw res;
          }
          return res['results'][0];
        })
        .retryWhen(errors => errors.pipe(
          tap(val => console.log(val)),
          delayWhen(val => timer(50))
        ))
        .do(res => console.log(res))
        .map(res => new Marker(res['geometry']['location'], res['formatted_address']));
  }

}

export class Marker {
  public name;
  constructor(public latLng: LatLngLiteral, public address: string) {

  }

}
