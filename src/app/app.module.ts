import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { AppComponent } from './component/app.component';
import { MapComponent } from './component/map.component';
import { AddressService } from './address.service';
import { HttpClient, HttpClientModule, HttpHandler} from '@angular/common/http';
import { AddressListComponent } from './component/address-list.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    AddressListComponent
  ],
  imports: [
      BrowserModule,
      FormsModule,
      HttpClientModule,
      AgmCoreModule.forRoot({
          apiKey: 'Insert key here. Get api key: https://developers.google.com/maps/documentation/javascript/get-api-key'
      }),
      NgbModule.forRoot()
  ],
  providers: [
      AddressService,
      HttpClient,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
