import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Marker} from '../address.service';

@Component({
  selector: 'app-address-list',
  template: `
      <table  class="table" *ngIf="markers.length>0">
          <tr>
              <th>Breddegrader</th>
              <th>Lengdegrader</th>
              <th>Adresse</th>
              <th>Navn</th>
          </tr>
          <tr *ngFor="let marker of markers; let i = index">
              <td>{{marker.latLng.lat}}</td>
              <td>{{marker.latLng.lng}}</td>
              <td>{{marker.address}}</td>
              <td><input type="text" class="form-control" placeholder="Skriv inn navn"
              [(ngModel)]="marker.name"
              ></td>
              <td><button (click)="deleteOnIndex(i)" >Slett</button></td>
            </tr>
      </table>
  `,
})
export class AddressListComponent implements OnInit {
  @Output() deleteIndex: EventEmitter<number> = new EventEmitter();
  @Input() markers: Marker[];
  constructor() { }

  ngOnInit() {}
  deleteOnIndex(index) {
    this.deleteIndex.emit(index);
  }

}
