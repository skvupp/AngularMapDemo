import { Component } from '@angular/core';
import {LatLngLiteral} from '@agm/core';
import {AddressService, Marker} from '../address.service';

@Component({
  selector: 'app-root',
  template: `
      <app-map [markers]="markers" (coords)="getAddress($event)" (changedIndex)="updateMarker($event)" 
               (deletedIndex)="deleteMarker($event)"></app-map>
      <app-address-list [markers]="markers" (deleteIndex)="deleteMarker($event)" ></app-address-list>
      `

})
export class AppComponent {
  markers: Marker[] = [];

  constructor(private addressService: AddressService) {}

  getAddress(coords: LatLngLiteral) {
    this.markers.push(new Marker(coords, ''));
    const index = this.markers.length - 1;

    this.addressService.getAddress(coords).subscribe(
        (data: Marker) => {
            if (!this.markers.some(value => value.address === data.address)) {
              this.markers[index] = data;
            }else {
              this.markers.pop();
            }
            },
            error => console.log(error),
            () => console.log(this.markers)
        );

  }

  updateMarker(index: number) {
      this.addressService.getAddress(this.markers[index].latLng).subscribe(
          (data: Marker) => {
              this.markers[index].address = data.address;
          },
          error => console.log(error),
          () => console.log(this.markers)

      );
  }

  deleteMarker(index: number) {
      this.markers.splice(index, 1);
  }

}
