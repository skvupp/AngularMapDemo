import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LatLngLiteral, MouseEvent} from '@agm/core';
import {Marker} from '../address.service';

@Component({
  selector: 'app-map',
  template: `
      <agm-map [latitude]="lat" [longitude]="lng" (mapClick)="mapClick($event)"
      [zoom]="18" [mapTypeId]="'hybrid'" (mapReady)="changeTilt($event)">
          <agm-marker *ngFor="let marker of markers; let i = index" [latitude]="marker.latLng.lat" 
                      [longitude]="marker.latLng.lng" [markerDraggable]="true" (dragEnd)="dragEnd($event, i)">
              <agm-info-window [isOpen]="true" *ngIf="marker.address!=''" >
                  <span *ngIf="marker.name">{{marker.name}},</span><br>
                  {{marker.address}}<br> <button (click)="deleteMarker(i)">slett</button>
              </agm-info-window>
              
          </agm-marker>
      </agm-map>
  `,
  styles: [`
      agm-map {
          height: 60vh;
          width: 100%;
      }`
  ]
})
export class MapComponent implements OnInit {
    @Output() coords: EventEmitter<LatLngLiteral> = new EventEmitter();
    @Output() changedIndex: EventEmitter<number> = new EventEmitter();
    @Output() deletedIndex: EventEmitter<number> = new EventEmitter();
    @Input() markers: Marker[];
    lat = 58.1559115;
    lng = 7.9800247;
    _map: any;

    constructor() { }

    ngOnInit() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position => {
                    this.lng = position.coords.longitude;
                    this.lat = position.coords.latitude;


                })
            );
        }
    }

    mapClick(e: MouseEvent) {
        this.coords.emit(e.coords);
    }

    dragEnd(e: MouseEvent, index: number) {
        this.markers[index].latLng = e.coords;
        this.changedIndex.emit(index);


    }

    changeTilt(map) {
        this._map = map;
        this._map.setTilt(0);
    }

    deleteMarker(index: number) {
        this.deletedIndex.emit(index);
    }

}
